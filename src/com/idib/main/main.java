package com.idib.main;

import com.idib.SDiagram.TDiagram;
import com.idib.Scanner.ScannerLeng;
import com.idib.Tree.Leaf;
import com.idib.Tree.Tree;

import java.awt.geom.Line2D;
import java.io.File;
import java.io.FileReader;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by idib on 29.09.17.
 */
public class main {
	public static void main(String[] args) throws Exception {
		File file = new File(main.class.getResource("input").toURI());
		File fileLecs = new File(main.class.getResource("lecs").toURI());
		File fileSem = new File(main.class.getResource("sample").toURI());
		List<ScannerLeng.Pair<Pattern, String>> tTypes = ScannerLeng.build(new FileReader(fileLecs));
//		ScannerLeng sc = new ScannerLeng(tTypes, new FileReader(file), System.out);
		ScannerLeng sc = new ScannerLeng(tTypes, new FileReader(file), null);
//		while (sc.has()) {
//			sc.GetLecs();
//		}
		TDiagram sd = new TDiagram(sc);
		try {
			sd.program(0);
			printTree(sd.root, 0);
		} catch (Exception e) {
//			e.printStackTrace();
			System.out.println(e.toString());
		}

		Line2D line = new Line2D.Double();


	}

	static void printTree(Tree a, int d) {
		String offer = cstr(d);
		for (Map.Entry<String, Leaf> stringLeafEntry : a.leafs.entrySet()) {
			System.out.println(offer + stringLeafEntry.getKey() + "->" + stringLeafEntry.getValue().type);
			if (stringLeafEntry.getValue().context != null)
				printTree(stringLeafEntry.getValue().context, d + 2);
		}
	}

	static String cstr(int n) {
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < n; i++) {
			str.append(" ");
		}
		return str.toString();
	}
}
