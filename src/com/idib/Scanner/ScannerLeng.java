package com.idib.Scanner;

import java.io.PrintStream;
import java.io.Reader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Created by idib on 29.09.17.
 */
public class ScannerLeng {
	private static Pattern scanPattern = Pattern.compile("[A-Za-z]\\w*|\\d+|'.'|[{\\}()\\[\\].,;]|\\+((?![=+])|=|\\+)|-((?![=-])|=|-)|(?<![\\/])\\*((?![=\\/])|=)|(?<![*\\/])\\/((?![=*\\/])|=)|%((?!=)|=)|<((?!=)|=)|>((?!=)|=)|=((?!=)|=)|!((?!=)|=)|&&|\\/\\/[^\\n]*|\\/\\*[^\\/]*\\*\\/|[^ \\t\\nA-Za-z0-9]+");
	public int ind = -1;
	public int countLine = 0;
	public int countLecs = 0;
	private Scanner sc;
	private PrintStream out;
	private Matcher mat;
	private int max;
	private List<Pair<Pattern, String>> listReg;
	private String tError = "TERROR";
	private String curLine = "";
	private List<String> memType = new ArrayList<>();
	private List<String> subStr = new ArrayList<>();
	private List<String> Str = new ArrayList<>();
	private List<Integer> indLine = new ArrayList<>();

	public ScannerLeng(List<Pair<Pattern, String>> tTypes, Reader reader, PrintStream out) {
		sc = new Scanner(reader);
		listReg = tTypes;
		this.out = out;
	}


	public static List<Pair<Pattern, String>> build(Reader reader) {
		Scanner sc = new Scanner(reader);
		List<Pair<Pattern, String>> list = new ArrayList<>();
		while (sc.hasNextLine()) {
			String[] arr = sc.nextLine().split("->");
			String reg = arr[0].replaceAll("\\\\-\\\\>", "->").trim();
			String type = arr[1].replaceAll("\\\\-\\\\>", "->").trim();
			if (isRegex(reg)) {
				Pattern pat = Pattern.compile("^" + reg + "$");
				list.add(new Pair<>(pat, type));
			}
		}

		return list;
	}

	private static boolean isRegex(String str) {
		try {
			Pattern.compile(str);
			return true;
		} catch (PatternSyntaxException e) {
			return false;
		}
	}

	private String getTypr(String str) {
		if (str.length() == 0)
			return "";
		for (Pair<Pattern, String> pair : listReg) {
			Pattern pat = pair.key;
			if (pat.matcher(str).find()) {
				return pair.value;
			}
		}
		return tError;
	}

	public boolean has() {
		return ind > 0 && ind < max || sc.hasNextLine();
	}

	private String next() {
		if (ind == -1 && sc.hasNextLine()) {
			curLine = sc.nextLine();
			mat = scanPattern.matcher(curLine);
			ind = 0;
			max = curLine.length();
			countLine++;
		}
		if (ind == -1 && !sc.hasNextLine())
			return "";
		if (mat.find(ind)) {
			countLecs++;
			ind = mat.end();
			Str.add(mat.group());
			return mat.group();
		}
		ind = -1;
		return next();
	}

	public String getLine() {
		if (ind < 0)
			ind = 0;
		return curLine.substring(0, ind);
	}

	public String GetLecs() {
		String str = next();
		String type = getTypr(str);
		subStr.add(getLine());
		memType.add(type);
		indLine.add(countLine);
		if (out != null)
			out.println(str + " --->>> " + type);
		return type;
	}

	public String getName(int i) {
		if (i < countLecs)
			return Str.get(i);
		else {
			String type = GetLecs();
			while (i >= countLecs && sc.hasNextLine()) {
				type = GetLecs();
			}
			return Str.get(i);
		}
	}


	public String GetLecs(int i) {
		if (i < countLecs)
			return memType.get(i);
		else {
			String type = GetLecs();
			while (i >= countLecs && sc.hasNextLine()) {
				type = GetLecs();
			}
			return type;
		}
	}

	public int getIndex(int dep) {
		if (dep < indLine.size())
			return indLine.get(dep);
		return -1;
	}

	public String getStr(int dep) {
		if (dep < subStr.size())
			return subStr.get(dep);
		return "";
	}

	public Set<String> converToStrs(Set<String> tmp) {
		Set<String> res = new HashSet<>();
		for (String s : tmp) {
			res.add(getNormStr(s));
		}
		return res;
	}

	public String getNormStr(String str) {
		for (Pair<Pattern, String> pair : listReg) {
			if (Objects.equals(str, pair.value))
				return pair.key.pattern();
		}
		return "";
	}

	public static class Pair<K, V> {
		public K key;
		public V value;

		public Pair(K key, V value) {
			this.key = key;
			this.value = value;
		}
	}
}