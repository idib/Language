package com.idib.Tree;

public class Leaf {
	public Type type;
	public Tree context;
	public int num = -1;

	public Leaf(Type type) {
		this.type = type;
	}

	public Leaf(Type type, Tree context) {
		this.type = type;
		this.context = context;
	}

	public Leaf(Type type, int num) {
		this.type = type;
		this.num = num;
	}

	public Leaf(Type objcet, Tree context, int num) {
		this.type = objcet;
		this.context = context;
		this.num = num;
	}
}