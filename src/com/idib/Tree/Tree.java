package com.idib.Tree;

import java.util.HashMap;
import java.util.Map;

public class Tree {
	public Tree parent;
	public Map<String, Leaf> leafs = new HashMap<>();


	public boolean contain(String name) {
		return leafs.containsKey(name);
	}

	public boolean find(String name) {
		return contain(name) || parent != null && parent.find(name);
	}

	public Leaf get(String name) {
		Leaf leaf = leafs.get(name);
		return leaf != null ? leaf : (parent != null ? parent.get(name) : null);
	}

	public void add(String name, Leaf leaf) {
		leafs.put(name, leaf);
	}
} 