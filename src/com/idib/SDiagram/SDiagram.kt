//package com.idib.SDiagram
//
//import com.idib.Scanner.ScannerLeng
//import java.io.Reader
//import java.util.*
//import java.util.regex.Pattern
//
///**
// * Created by idib on 18.10.17.
// */
//
//class SDiagram(val scannerLeng: ScannerLeng, val reader: Reader) {
//    class Node(var name: String = "") {
//        var end: Node? = null
//        val child = mutableListOf<Node>()
//        var rchild: Node? = null
//        var isEnd = true
//
//        fun addAll(node: Node) {
//            isEnd = false
//            child.add(node)
//            node.end = end
//            for (i in node.child) {
//                i.end = end
//            }
//        }
//
//        fun setEnds(node: Node) {
//            for (i in child) {
//                i.setEnds(node)
//            }
//            if (isEnd)
//                child.add(node)
//        }
//
//        fun cfind(str: String): Boolean {
//            var fl = false
//            for (i in child) {
//                    if (i.name == "")
//                        fl = fl || i.find(str)
//                if (i.name == str)
//                    fl = true
//            }
//            return fl
//        }
//
//        fun find(str: String): Boolean {
//            return rfind(str) || cfind(str)
//        }
//
//        fun rfind(str: String): Boolean {
//            if (rchild != null) {
//                if (rchild!!.name == str)
//                    return true
//                return rchild!!.find(str)
//            }
//            return false
//        }
//
//        fun countfind(str: String): Int {
//            var fl = 0
//            for (i in child) {
//                if (i.name == str || i.name == "" && i.find(str))
//                    fl += 1
//            }
//            return fl
//        }
//
//        fun goto(str: String): Node? {
//            for (i in child)
//                if (i.name == str || i.name == "" && i.find(str))
//                    return i
//            return null
//        }
//
//        fun listVar(): Set<String> {
//            var fl = mutableSetOf<String>()
//            if (rchild == null)
//                for (i in child)
//                    if (i.name == "")
//                        fl.addAll(i.listVar())
//                    else
//                        fl.add(i.name)
//            else
//                fl.addAll(rchild!!.listVar())
//            return fl
//        }
//
//        fun clistVar(): Set<String> {
//            var fl = mutableSetOf<String>()
//            for (i in child)
//                if (i.name == "")
//                    fl.addAll(i.listVar())
//                else
//                    fl.add(i.name)
//            return fl
//        }
//
//        fun run(sc: ScannerLeng, strs: String = "", startDepth: Int = 0) {
//            if (!sc.has() && startDepth < sc.countLecs) return
//            var str = strs
//            var depth = startDepth
//            if (str == name)
//                str = ""
//            var fl = false
//
//            if (rchild != null) {
//                try {
//                    rchild!!.run(sc, str, depth)
//                } catch (e: Exception) {
//                    if (e.message == null)
//                        throw e
//                    str = e.message!!
//                    fl = true
//                }
//            }
//
//            if (str == "") {
//                str = sc.GetLecs(depth++)
//            }
//
//            val count = countfind(str)
//            if (count == 1)
//                return goto(str)!!.run(sc, str, depth)
//            if (count > 1) {
//                var lExc = mutableListOf<strException>()
//                for (i in child) {
//                    if (i.name == str || i.find(str)) {
//                        try {
//                            return goto(str)!!.run(sc, str, depth)
//                        } catch (e: strException) {
//                            lExc.add(e)
//                        }
//                    }
//                }
//                for (i in lExc) {
//                    println(i)
//                }
//            }
//
//
//            var list: Set<String>
//            if (fl)
//                list = clistVar()
//            else
//                list = listVar()
//            if (list.isEmpty())
//                throw Exception(str)
//            throw strException(sc, list, depth)
//
//        }
//    }
//
//
//
//    val child = mutableMapOf<String, Node>()
//    var root: Node
//    var nextId: Int = 1
//    val use = mutableSetOf<Node>()
//    val techName = mutableListOf<Node>()
//
//    init {
//        var sc = Scanner(reader)
//        var str = ""
//        while (sc.hasNextLine())
//            str += sc.nextLine()
//        str.split(Regex("\\{[^\\}]*\\}")).forEach { it ->
//            child[it.trim()] = Node(it.trim())
//        }
//        var arr = str.split(Regex("\\{|\\}")).map(String::trim).filter { it.length > 0 }
//        root = child[arr[0]] as Node
//        for (i in arr.indices step 2) {
//            techName.add(get(arr[i]))
//            buildNode(arr[i + 1], arr[i])
//        }
//        delTemp()
//        optimize(root)
//        println()
//    }
//
//    public fun run() {
//        try {
//            root.run(scannerLeng)
//            if (scannerLeng.has())
//                println("Больше ничего не ожидается")
//        } catch (e: Exception) {
//
//        }
//    }
//
//    private fun get(name: String): Node {
//        if (!child.contains(name)) {
//            child[name] = Node(name)
//            child[name]!!.end = Node()
//        }
//        return child[name]!!
//    }
//
//    private fun techGet(name: String): Node {
//        if (!child.contains(name)) {
//            child[name] = Node(name)
//            child[name]!!.end = Node()
//            techName.add(child[name]!!)
//        }
//        return child[name]!!
//    }
//
//    private fun genName(): String {
//        while (child.contains(nextId.toString())) nextId++
//        return nextId.toString()
//    }
//
//    private fun delTemp() {
//        for (nod in techName) {
//            nod.name = ""
//        }
//    }
//
//    private fun buildNode(description: String, name: String = ""): Node {
//        val start: Node
//        if (name.isNotEmpty())
//            start = get(name)
//        else {
//            start = Node("")
//        }
//        var str = description
//
//        var i = 0
//        while (i < str.length) {
//            if (str[i] == '(') {
//                var end = findBracker(str, i)
//                var newName = genName()
//                techGet(newName)
//                buildNode(str.substring(i + 1, end - 1), newName)
//                var s = ""
//                if (i - 1 > 0)
//                    i--
//                s = str.substring(0, i) + "[" + newName + "]" + str.substring(end)
//                str = s
//            }
//            i++
//        }
//
//        var tempArr = str.split("|")
//        if (tempArr.size > 1) {
//            for (it in tempArr) {
//                if (it.trim() == "") {
//                    var tempNode = Node()
//                    tempNode.end = start.end
//                    start.addAll(tempNode)
//                } else {
//                    get(it.trim()).end = start.end
//                    val n = buildNode(it.trim())
//                    start.addAll(n)
//                }
//            }
//        } else {
//            //todo TType replace native char
//
//            var reg = Pattern.compile(">\\d+|<[^>]+\\d+>|\\[[^]]+\\]|\\w+")
//            var cur = start
//            var mem: Node? = null
//            var memInit = false
//            var mat = reg.matcher(str)
//            var ind = 0
//            while (mat.find(ind)) {
//                ind = mat.end()
//                var com = mat.group()
//                if (Regex("^\\[[^]]+\\]$").matches(com)) {
//                    var t = get(com.substring(1, com.length - 1))
//                    var temp = Node()
//                    cur.addAll(temp)
//                    cur = temp
//                    cur.rchild = t
//                } else if (Regex("^[a-zA-z]+$").matches(com)) {
//                    var temp = Node(com)
//                    cur.addAll(temp)
//                    cur = temp
//                } else if (Regex("^>\\d+$").matches(com)) {
//                    mem = techGet(com)
//                    memInit = true
//                } else if (Regex("^<[^>]+\\d+>$").matches(com)) {
//                    var i = com.length - 2
//                    while (i >= 1 && com[i].isDigit()) i--
//                    var sub = com.substring(i + 1, com.length - 1)
//                    techGet(">" + sub)
//                    var t = buildNode(com.substring(1, i), ">" + sub)
//                    var endss = Node()
//                    t.setEnds(t)
//                    cur.child.add(t)
//                    cur.isEnd = false
//                    cur.addAll(endss)
//                    cur = endss
//                }
//                if (mem != null && !memInit) {
//                    mem.end!!.child.addAll(cur.child)
//                    mem.isEnd = false
//                    mem = null
//                }
//                if (memInit) {
//                    memInit = false
//                }
//            }
//        }
//        return start
//    }
//
//    private fun optimize(node: Node) {
//        opt1(node)
//        use.clear()
//        opt2(node)
//    }
//
//    fun opt1(node: Node) {
//        use.add(node)
//        if (node.end != null && isEmpty(node.end!!))
//            node.end = null
//
//        if (node.isEnd && node.end != null) {
//            node.child.addAll(node.end!!.child)
//        }
//
//        if (node.rchild != null && node.rchild!!.child.size == 1)
//            node.rchild = node.rchild!!.child[0]
//
//        for (nodeq in node.child)
//            if (!use.contains(nodeq)) {
//                opt1(nodeq)
//            }
//    }
//
//    fun opt2(node: Node) {
//        use.add(node)
//        var i = 0
//        while (i < node.child.size) {
//            if (node.child[i].name == "" && !node.child[i].isEnd && node.child[i].rchild == null) {
//                var t = node.child[i]
//                node.child.removeAt(i)
//                node.child.addAll(t.child)
//                i = -1
//            }
//            i++
//        }
//
//        for (nodeq in node.child)
//            if (!use.contains(nodeq)) {
//                opt2(nodeq)
//            }
//        if (node.rchild != null && !use.contains(node.rchild!!)) {
//            opt2(node.rchild!!)
//        }
//    }
//
//    private fun findBracker(str: String, ind: Int): Int {
//        var balance = 0
//        var i = ind
//        do {
//            if (str[i] == '(')
//                balance++
//            if (str[i] == ')')
//                balance--
//            i++
//        } while (balance != 0)
//        return i
//    }
//
//    private fun isEmpty(node: Node): Boolean {
//        return node.child.isNotEmpty() || node.rchild != null
//    }
//}