package com.idib.SDiagram

import com.idib.Scanner.ScannerLeng

/**
 * Created by idib on 26.10.17.
 */
class strException(val sc: ScannerLeng, val list: Set<String>, val depth: Int) : Exception() {
    override fun toString(): String {
        var i = sc.getIndex(depth)
        var str = sc.getStr(depth)
        return "Ошибка в строке №$i $str < недопустимо. Ожидается $list"
    }
}


class castomExp(val promt: String, val sc: ScannerLeng, val depth: Int) : Exception() {
    override fun toString(): String {
        var i = sc.getIndex(depth)
        var str = sc.getStr(depth)
        return "Ошибка в строке №$i $str <. $promt"
    }
}
