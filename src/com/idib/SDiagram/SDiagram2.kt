//package com.idib.SDiagram
//
//import com.idib.Scanner.ScannerLeng
//import java.io.Reader
//import java.util.*
//import java.util.regex.Pattern
//
///**
// * Created by idib on 18.10.17.
// */
//
//class SDiagram2(val scannerLeng: ScannerLeng, val reader: Reader) {
//
//    abstract class Node {
//        var isEnd = true
//        var next: Node? = null
//        open fun add(node: Node) {
//            isEnd = false
//        }
//
//        abstract fun setEnds(node: Node)
//
//        abstract fun find(str: String): Boolean
//
//        abstract fun listVar(): Set<String>
//
//        abstract fun run(sc: ScannerLeng, startDepth: Int = 0)
//    }
//
//    class TNode(val text: String) : Node() {
//        override fun add(node: Node) {
//            next = node
//            super.add(node)
//        }
//
//        override fun setEnds(node: Node) {
//            if (isEnd)
//                next = node
//            else
//                next?.setEnds(node)
//        }
//
//        override fun find(str: String): Boolean {
//            return text == str
//        }
//
//        override fun listVar(): Set<String> {
//            return mutableSetOf(text)
//        }
//
//        override fun run(sc: ScannerLeng, startDepth: Int) {
//            if (find(sc.GetLecs(startDepth)))
//                next?.run(sc, startDepth + 1)
//            else
//                throw ResEx(sc, listVar(), startDepth)
//        }
//
//    }
//
//    class OrNode(var F: MutableList<Node>) : Node() {
//        var nullable = false
//
//        override fun setEnds(node: Node) {
//            for (i in F)
//                setEnds(node)
//        }
//
//        override fun add(node: Node) {
//
//            super.add(node)
//        }
//
//        override fun find(str: String): Boolean {
//            var res = false
//            for (i in F)
//                res = res || find(str)
//            return res
//        }
//
//        override fun listVar(): Set<String> {
//            var list = mutableSetOf<String>()
//            for (i in F)
//                list.addAll(i.listVar())
//            return list
//        }
//
//        override fun run(sc: ScannerLeng, startDepth: Int) {
//            var list = mutableSetOf<String>()
//            var fl = false
//            for (i in F) {
//                if (i.find(sc.GetLecs(startDepth))) {
//                    try {
//                        if (next != null)
//                            i.setEnds(next!!)
//                        i.run(sc, startDepth + 1)
//                        fl = true
//                    } catch (e: ResEx) {
//                        list.add(e.message!!)
//                    }
//                }
//            }
//            if (!fl && nullable) {
//                try {
//                    next?.run(sc, startDepth + 1)
//                    fl = true
//                } catch (e: ResEx) {
//                    list.add(e.message!!)
//                }
//            }
//            if (!fl) {
//                for (i in list)
//                    println(i)
//            }
//        }
//
//    }
//
//    class FNode(var F: Node) : Node() {
//        override fun setEnds(node: Node) {
//            if (isEnd)
//                next = node
//            else
//                next?.setEnds(node)
//        }
//
//        override fun find(str: String): Boolean {
//            return F.find(str)
//        }
//
//        override fun listVar(): Set<String> {
//            return F.listVar()
//        }
//
//        override fun run(sc: ScannerLeng, startDepth: Int) {
//            if (next != null)
//                F.setEnds(next!!)
//            run(sc, startDepth + 1)
//        }
//    }
//
//
//    class ResEx(val sc: ScannerLeng, val list: Set<String>, val depth: Int) : Exception() {
//        override fun toString(): String {
//            var i = sc.getIndex(depth)
//            var str = sc.getStr(depth)
//            return "Ошибка в строке №" + i + " " + str + " < недопустимо. Ожидается " + list;
//        }
//    }
//
//    val child = mutableMapOf<String, Node>()
//    var root: Node
//    var nextId: Int = 1
//    val use = mutableSetOf<Node>()
//    val techName = mutableListOf<Node>()
//
//    init {
//        var sc = Scanner(reader)
//        var str = ""
//        while (sc.hasNextLine())
//            str += sc.nextLine()
//        var arr = str.split(Regex("\\{|\\}")).map(String::trim).filter { it.length > 0 }
//        for (i in arr.indices step 2) {
//            techName.add(get(arr[i]))
//            child[arr[i]] = buildNode(arr[i + 1], arr[i])
//        }
//        root = child[arr[0]] as Node
//        delTemp()
//        optimize(root)
//        println()
//    }
//
//    public fun run() {
//        try {
//            root.run(scannerLeng)
//            if (scannerLeng.has())
//                println("Больше ничего не ожидается")
//        } catch (e: Exception) {
//        }
//    }
//
//    private fun get(name: String): Node {
//        if (!child.contains(name)) {
//            child[name] = Node(name)
//            child[name]!!.end = Node()
//        }
//        return child[name]!!
//    }
//
//    private fun techGet(name: String): Node {
//        if (!child.contains(name)) {
//            child[name] = Node(name)
//            child[name]!!.end = Node()
//            techName.add(child[name]!!)
//        }
//        return child[name]!!
//    }
//
//    private fun genName(): String {
//        while (child.contains(nextId.toString())) nextId++
//        return nextId.toString()
//    }
//
//    private fun buildNode(description: String, name: String = ""): Node {
//        val start: Node
//        if (name.isNotEmpty())
//            start = get(name)
//        else {
//            start = Node("")
//        }
//        var str = description
//
//        var i = 0
//        while (i < str.length) {
//            if (str[i] == '(') {
//                var end = findBracker(str, i)
//                var newName = genName()
//                techGet(newName)
//                buildNode(str.substring(i + 1, end - 1), newName)
//                var s = ""
//                if (i - 1 > 0)
//                    i--
//                s = str.substring(0, i) + ":" + newName  + str.substring(end)
//                str = s
//            }
//            i++
//        }
//
//        var tempArr = str.split("|")
//        if (tempArr.size > 1) {
//            var list = mutableListOf<Node>()
//            var tempNode = OrNode(list)
//            for (it in tempArr) {
//                if (it.trim() == "") {
//                    tempNode.nullable = true
//                } else {
//                    tempNode.add(buildNode(it.trim()))
//                }
//            }
//            return tempNode
//        } else {
//            //todo TType replace native char
//
//            var reg = Pattern.compile(">\\d+|<[^>]+\\d+>|\\[[^]]+\\]|\\w+")
//            var cur = start
//            var mem: Node? = null
//            var memInit = false
//            var mat = reg.matcher(str)
//            var ind = 0
//            while (mat.find(ind)) {
//                ind = mat.end()
//                var com = mat.group()
//                if (Regex("^\\[[^]]+\\]$").matches(com)) {
//                    var t = get(com.substring(1, com.length - 1))
//                    var temp = Node()
//                    cur.addAll(temp)
//                    cur = temp
//                    cur.rchild = t
//                } else if (Regex("^[a-zA-z]+$").matches(com)) {
//                    var temp = Node(com)
//                    cur.addAll(temp)
//                    cur = temp
//                } else if (Regex("^>\\d+$").matches(com)) {
//                    mem = techGet(com)
//                    memInit = true
//                } else if (Regex("^<[^>]+\\d+>$").matches(com)) {
//                    var i = com.length - 2
//                    while (i >= 1 && com[i].isDigit()) i--
//                    var sub = com.substring(i + 1, com.length - 1)
//                    techGet(">" + sub)
//                    var t = buildNode(com.substring(1, i), ">" + sub)
//                    var endss = Node()
//                    t.setEnds(t)
//                    cur.child.add(t)
//                    cur.isEnd = false
//                    cur.addAll(endss)
//                    cur = endss
//                }
//                if (mem != null && !memInit) {
//                    mem.end!!.child.addAll(cur.child)
//                    mem.isEnd = false
//                    mem = null
//                }
//                if (memInit) {
//                    memInit = false
//                }
//            }
//        }
//        return start
//    }
//
//    private fun optimize(node: Node) {
//        opt1(node)
//        use.clear()
//        opt2(node)
//    }
//
//    fun opt1(node: Node) {
//        use.add(node)
//        if (node.end != null && isEmpty(node.end!!))
//            node.end = null
//
//        if (node.isEnd && node.end != null) {
//            node.child.addAll(node.end!!.child)
//        }
//
//        if (node.rchild != null && node.rchild!!.child.size == 1)
//            node.rchild = node.rchild!!.child[0]
//
//        for (nodeq in node.child)
//            if (!use.contains(nodeq)) {
//                opt1(nodeq)
//            }
//    }
//
//    fun opt2(node: Node) {
//        use.add(node)
//        var i = 0
//        while (i < node.child.size) {
//            if (node.child[i].name == "" && !node.child[i].isEnd && node.child[i].rchild == null) {
//                var t = node.child[i]
//                node.child.removeAt(i)
//                node.child.addAll(t.child)
//                i = -1
//            }
//            i++
//        }
//
//        for (nodeq in node.child)
//            if (!use.contains(nodeq)) {
//                opt2(nodeq)
//            }
//        if (node.rchild != null && !use.contains(node.rchild!!)) {
//            opt2(node.rchild!!)
//        }
//    }
//
//    private fun findBracker(str: String, ind: Int): Int {
//        var balance = 0
//        var i = ind
//        do {
//            if (str[i] == '(')
//                balance++
//            if (str[i] == ')')
//                balance--
//            i++
//        } while (balance != 0)
//        return i
//    }
//
//    private fun isEmpty(node: Node): Boolean {
//        return node.child.isNotEmpty() || node.rchild != null
//    }
//}