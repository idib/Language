package com.idib.SDiagram;

import com.idib.Scanner.ScannerLeng;
import com.idib.Tree.Leaf;
import com.idib.Tree.Tree;
import com.idib.Tree.Type;

import java.util.*;

/**
 * Created by idib on 26.10.17.
 */

public class TDiagram {
	private ScannerLeng sc;
	public Tree root = new Tree();

	private boolean checkContext = false;

	public TDiagram(ScannerLeng sc) {
		this.sc = sc;
	}

	private strException comboEx(strException... a) {
		int max = -1;
		for (strException strException : a) {
			max = Math.max(strException.getDepth(), max);
		}
		Set<String> list = new HashSet<>();
		for (strException strException : a) {
			if (strException.getDepth() == max)
				list.addAll(strException.getList());
		}
		return new strException(sc, list, max);
	}

	private strException oneException(ScannerLeng sc, String list, int depth) {
		return new strException(sc, new HashSet<>(Arrays.asList(list)), depth);
	}

	private void assertOne(String name, String str, int depth) throws strException {
		if (!name.equals(str)) throw oneException(sc, str, depth);
	}

	private void assertMore(String name, int depth, String... a) throws strException {
		Set<String> tmp = new HashSet<>(Arrays.asList(a));
		if (!tmp.contains(name)) throw new strException(sc, tmp, depth);
	}

	private boolean testMore(String name, String... a) throws strException {
		Set<String> tmp = new HashSet<>(Arrays.asList(a));
		return tmp.contains(name);
	}

	public void program(int depth) throws strException, castomExp {
		String name = sc.GetLecs(depth);
		Set<String> tmp = new HashSet<>();
		tmp.add("TClass");
		tmp.add("TInt");
		tmp.add("TChar");
		tmp.add("TID");
		while (tmp.contains(name)) {
			switch (name) {
				case "TClass":
					depth = sampleClass(depth);
					break;
				case "TInt":
				case "TChar":
				case "TID":
					try {
						depth = data(depth);
					} catch (strException e1) {
						try {
							depth = func(depth);
						} catch (strException e2) {
							throw comboEx(e1, e2);
						}
					}
					break;
			}
			name = sc.GetLecs(depth);
		}
		if (!name.isEmpty())
			throw new strException(sc, tmp, depth);
	}

	private int sampleClass(int depth) throws strException, castomExp {
		String name = sc.GetLecs(depth);
		assertOne(name, "TClass", depth);

		name = sc.GetLecs(++depth);
		String tid = sc.getName(depth);
		assertOne(name, "TID", depth);
		int memtid = depth;


		name = sc.GetLecs(++depth);
		assertOne(name, "TBraceL", depth);

		Tree mem = root;
		root = new Tree();
		Leaf leaf = new Leaf(Type.CLASS, root);
		root.parent = mem;

		depth = classCode(++depth);


		name = sc.GetLecs(depth);
		assertOne(name, "TBraceR", depth);
		root = mem;

		if (checkContext) {
			tused(tid, memtid);
			mem.add(tid, leaf);
		}
		return ++depth;
	}

	private void tused(String name, int depth) throws castomExp {
		if (root.contain(name))
			throw new castomExp("В дереве уже существует " + name, sc, depth);
	}

	private void tunused(String name, int depth) throws castomExp {
		if (!root.contain(name))
			throw new castomExp("В дереве еще не существует " + name, sc, depth);
	}

	private void tcount(int depth) throws castomExp {
		throw new castomExp("Количество аргументов не соответствует ", sc, depth);
	}

	private int classCode(int depth) throws strException, castomExp {
		String name = sc.GetLecs(depth);
		Set<String> tmp = new HashSet<>();
		tmp.add("TInt");
		tmp.add("TChar");
		tmp.add("TID");
		while (tmp.contains(name)) {
			try {
				depth = data(depth);
			} catch (strException e1) {
				try {
					depth = func(depth);
				} catch (strException e2) {
					throw comboEx(e1, e2);
				}
			}
			name = sc.GetLecs(depth);
		}
		return depth;
	}


	private int data(int depth) throws strException, castomExp {
		String name = sc.GetLecs(depth);
		String memT = name;
		Set<String> tmp = new HashSet<>();
		tmp.add("TInt");
		tmp.add("TChar");
		tmp.add("TID");
		Set<String> memor = new HashSet<>();
		if (!tmp.contains(name)) throw new strException(sc, tmp, depth);

		String tid2 = sc.getName(depth);
		int memtid2 = depth;

		List<Triple<String, Leaf, Integer>> memsID = new ArrayList<>();

		do {
			name = sc.GetLecs(++depth);
			String tid = sc.getName(depth);
			assertOne(name, "TID", depth);

			int memD = depth;

			name = sc.GetLecs(++depth);

			Pair<Integer, Type> res = null;
			if (Objects.equals(name, "TAssign")) {
				res = or(++depth);
				depth = res.first;
				name = sc.GetLecs(depth);
			}

			Leaf leaf;
			if (checkContext) {
				if (memT.equals("TID")) {
					leaf = root.get(tid2);
					getTest(leaf, tid2, depth);
					leaf = new Leaf(Type.Objcet, leaf.context);
				} else if (memT.equals("TInt"))
					leaf = new Leaf(Type.INT);
				else
					leaf = new Leaf(Type.CHAR);
				if (res != null)
					ExpAssing(leaf.type, res.second, depth);
				memsID.add(new Triple<>(tid, leaf, memD));
			}

		}
		while (Objects.equals(name, "TComma"));

		assertOne(name, "TSemicolon", depth);

		if (checkContext) {
			if (memT.equals("TID")) {
				getTest(root.get(tid2), tid2, memtid2);
			}
			for (Triple<String, Leaf, Integer> triple : memsID) {
				tused(triple.first, triple.trid);
				root.add(triple.first, triple.second);
			}
		}
		return ++depth;
	}

	private int func(int depth) throws strException, castomExp {
		String name = sc.GetLecs(depth);
		String memretT = name;
		assertMore(name, depth, "TInt", "TChar", "TID");
		String tid2 = sc.getName(depth);
		int memtid2 = depth;

		name = sc.GetLecs(++depth);
		String tid = sc.getName(depth);
		assertMore(name, depth, "TID", "TMain");
		int memtid = depth;


		name = sc.GetLecs(++depth);
		assertOne(name, "TBracketL", depth);

		Tree mem = root;
		root = new Tree();
		Leaf leaf = new Leaf(Type.FUNC, root);
		root.parent = mem;

		name = sc.GetLecs(++depth);

		if (testMore(name, "TInt", "TChar", "TID")) {
			depth = params(depth);
			name = sc.GetLecs(depth);
		}

		assertOne(name, "TBracketR", depth);

		depth = block(++depth);

		root = mem;

		if (checkContext) {
			if (memretT.equals("TID")) {
				tunused(tid2, memtid2);
			}
			tused(tid, memtid);
			mem.add(tid, leaf);
		}
		return depth;
	}

	private Pair<Integer, Type> or(int depth) throws strException, castomExp {
		String name;
		Pair<Integer, Type> res;
		res = and(depth);
		depth = res.first;
		name = sc.GetLecs(depth);
		boolean fl = false;
		while (Objects.equals(name, "TOr")) {
			res = and(++depth);
			depth = res.first;
			fl = true;
			name = sc.GetLecs(depth);
		}

		if (fl)
			res.second = Type.INT;
		return res;
	}

	private Pair<Integer, Type> and(int depth) throws strException, castomExp {
		String name;
		Pair<Integer, Type> res;
		res = eq(depth);
		depth = res.first;
		name = sc.GetLecs(depth);
		boolean fl = false;
		while (Objects.equals(name, "TAnd")) {
			res = eq(++depth);
			depth = res.first;
			name = sc.GetLecs(depth);
			fl = true;
		}

		if (fl)
			res.second = Type.INT;
		return res;
	}

	private Pair<Integer, Type> eq(int depth) throws strException, castomExp {
		String name;
		Pair<Integer, Type> res;
		res = comp(depth);
		depth = res.first;
		name = sc.GetLecs(depth);
		boolean fl = false;
		while (testMore(name, "TEquale", "TNotEquale")) {
			res = comp(++depth);
			depth = res.first;
			name = sc.GetLecs(depth);
			fl = true;
		}

		if (fl)
			res.second = Type.INT;
		return res;
	}

	private Pair<Integer, Type> comp(int depth) throws strException, castomExp {
		String name;
		Pair<Integer, Type> res;
		res = pm(depth);
		depth = res.first;
		name = sc.GetLecs(depth);
		boolean fl = false;
		while (testMore(name, "TMore", "TLess", "TMoreOrEqale", "TlessOrEqale")) {
			res = pm(++depth);
			depth = res.first;
			name = sc.GetLecs(depth);
			fl = true;
		}

		if (fl)
			res.second = Type.INT;
		return res;
	}

	private Pair<Integer, Type> pm(int depth) throws strException, castomExp {
		String name;
		Pair<Integer, Type> res;
		res = md(depth);
		depth = res.first;
		Type last = res.second;
		name = sc.GetLecs(depth);
		while (testMore(name, "TPlus", "TMinus")) {
			res = md(++depth);
			name = sc.GetLecs(depth);
			depth = res.first;
			if (last == Type.CLASS)
				res.second = last;
		}
		return res;
	}

	private Pair<Integer, Type> md(int depth) throws strException, castomExp {
		String name;
		Pair<Integer, Type> res;
		res = prefpm(depth);
		depth = res.first;
		Type last = res.second;
		name = sc.GetLecs(depth);
		while (testMore(name, "TMultiply", "TDivision", "TMod")) {
			res = prefpm(++depth);
			name = sc.GetLecs(depth);
			depth = res.first;
			if (last == Type.CLASS)
				res.second = last;
		}
		return res;
	}

	private Pair<Integer, Type> prefpm(int depth) throws strException, castomExp {
		String name = sc.GetLecs(depth);
		if (testMore(name, "TPlusPlus", "TMinusMinus", "TPlus", "TMinus"))
			++depth;
		return sufpm(depth);
	}

	private Pair<Integer, Type> sufpm(int depth) throws strException, castomExp {
		Pair<Integer, Type> res;
		res = exp(depth);
		String name = sc.GetLecs(res.first);
		if (testMore(name, "TPlusPlus", "TMinusMinus"))
			++res.first;
		return res;
	}

	private Pair<Integer, Type> exp(int depth) throws strException, castomExp {
		Pair<Integer, Type> res;
		String name = sc.GetLecs(depth);
		if (Objects.equals(name, "TBracketL")) {
			res = or(++depth);
			depth = res.first;
			assertOne(name, "TBracketR", depth);
		} else if (testMore(name, "TInt10", "TInt16", "TCharC")) {
			depth++;
			res = new Pair<>(depth, Type.INT);
		} else {
			try {
				res = funcCall(depth);
			} catch (strException e) {
				try {
					res = comboId(depth);
				} catch (strException e2) {
					throw comboEx(e, e2);
				}
			}
		}
		return res;
	}

	void getTest(Object obj, String name, int depth) throws castomExp {
		if (obj == null)
			throw new castomExp("В дереве еще не существует " + name, sc, depth);
	}

	private Pair<Integer, Type> comboId(int depth) throws strException, castomExp {
		String name, tid;
		Leaf last;
		Stack<Tree> treeStack = new Stack<>();
		--depth;
		boolean fl = false;
		Type t = null;
		do {
			name = sc.GetLecs(++depth);
			tid = sc.getName(depth);
			assertOne(name, "TID", depth);
			if (checkContext) {
				getTest(root, tid, depth);
				last = root.get(tid);
				getTest(last, tid, depth);
				treeStack.push(root);
				root = last.context;
				t = last.type;
			}

			name = sc.GetLecs(++depth);
//			if (Objects.equals(name, "TSBracketL")) {
//				assertOne(name, "TSBracketL", depth);
//
//				depth = or(++depth);
//
//				name = sc.GetLecs(++depth);
//				assertOne(name, "TSBracketR", depth);
//				name = sc.GetLecs(++depth);
//			}
			fl = true;
		} while (Objects.equals(name, "Tdot"));
		if (checkContext) {
			while (!treeStack.empty())
				root = treeStack.pop();
		}
		return new Pair<>(depth, t);
	}

	private int params(int depth) throws strException, castomExp {
		String name;
		--depth;
		int count = 0;
		do {
			depth = param(++depth, count++);
			name = sc.GetLecs(depth);
		} while (Objects.equals(name, "TComma"));
		return depth;
	}

	private int param(int depth, int num) throws strException, castomExp {
		String name = sc.GetLecs(depth);
		assertMore(name, depth, "TInt", "TChar", "TID");
		String tid2 = sc.getName(depth);
		String ttid2 = name;
		if (checkContext) {
			if (name.equals("TID")) {
				getTest(root, tid2, depth);
				getTest(root.get(tid2), tid2, depth);
				getTest(root.get(tid2).context, tid2, depth);
			}
		}
		name = sc.GetLecs(++depth);
		assertOne(name, "TID", depth);
		String tid = sc.getName(depth);
		int memtid = depth;

		Leaf leaf;
		if (checkContext) {

			if (ttid2.equals("TID"))
				leaf = new Leaf(Type.Objcet, root.get(tid2).context, num);
			else if (ttid2.equals("TInt"))
				leaf = new Leaf(Type.INT, num);
			else
				leaf = new Leaf(Type.CHAR, num);

			tused(tid, memtid);
			root.add(tid, leaf);
		}
		return ++depth;
	}

	private int block(int depth) throws strException, castomExp {
		Tree mem = root;
		root = new Tree();
		Leaf leaf = new Leaf(Type.FUNC, root);
		root.parent = mem;
		depth = block(depth, leaf);
		if (checkContext)
			mem.add("", leaf);
		root = mem;
		return depth;
	}

	private int block(int depth, Leaf leaf) throws strException, castomExp {
		String name = sc.GetLecs(depth);
		assertOne(name, "TBraceL", depth);
		depth = operators(++depth);
		name = sc.GetLecs(depth);
		assertOne(name, "TBraceR", depth);
		return depth + 1;
	}

	private int operators(int depth) throws strException, castomExp {
		String name = sc.GetLecs(depth);
		while (testMore(name, "TIf", "TBraceL", "TChar", "TInt", "TID", "TSemicolon")) {
			try {
				depth = data(depth);
			} catch (strException e1) {
				try {
					depth = operator(depth);
				} catch (strException e2) {
					throw comboEx(e1, e2);
				}
			}
			name = sc.GetLecs(depth);
		}
		return depth;
	}

	private int operator(int depth) throws strException, castomExp {
		String name = sc.GetLecs(depth);
		if (Objects.equals(name, "TSemicolon")) {
			++depth;
		} else {
			try {
				depth = tryIf(depth);
			} catch (strException e1) {
				try {
					depth = testSemicolon(assign(depth));
				} catch (strException e2) {
					try {
						depth = block(depth);
					} catch (strException e3) {
						try {
							depth = testSemicolon(funcCall(depth).first);
						} catch (strException e4) {
							throw comboEx(e1, e2, e3, e4);
						}
					}
				}
			}
		}
		return depth;
	}

	private int testSemicolon(int depth) throws strException {
		String name = sc.GetLecs(depth);
		assertOne(name, "TSemicolon", depth);
		return ++depth;
	}

	private int tryIf(int depth) throws strException, castomExp {
		String name = sc.GetLecs(depth);
		assertOne(name, "TIf", depth);

		name = sc.GetLecs(++depth);
		assertOne(name, "TBracketL", depth);

		depth = or(++depth).first;

		name = sc.GetLecs(depth);
		assertOne(name, "TBracketR", depth);

		depth = operator(++depth);

		name = sc.GetLecs(depth);

		if (Objects.equals(name, "TElse")) {
			assertOne(name, "TElse", depth);
			depth = operator(++depth);
		}

		return depth;
	}

	private int assign(int depth) throws strException, castomExp {
		Pair<Integer, Type> res = comboId(depth);
		depth = res.first;
		String name = sc.GetLecs(depth);
		assertOne(name, "TAssign", depth);
		Pair<Integer, Type> res2 = or(++depth);
		ExpAssing(res.second, res2.second, depth);
		return res2.first;
	}

	private void ExpAssing(Type second, Type second1, int depth) throws castomExp {
		if (second != second1 && (second == Type.Objcet || second == Type.INT || second == Type.CHAR))
			throw new castomExp("Не возможно преведение типа", sc, depth);
	}


	private Pair<Integer, Type> funcCall(int depth) throws strException, castomExp {
		String name = sc.GetLecs(depth);
		assertOne(name, "TID", depth);
		String tid = sc.getName(depth);
		int memDepth = depth;
		name = sc.GetLecs(++depth);
		Pair<Integer, Type> res = new Pair<>(depth, null);

		assertOne(name, "TBracketL", depth);

		Tree tmp = null;

		if (checkContext) {
			tmp = root.get(tid).context;
			getTest(root, tid, depth);
			res.second = root.get(tid).type;
		}
		name = sc.GetLecs(++depth);
		if (!Objects.equals(name, "TBracketR")) {
			depth = fparams(depth, tmp);
			name = sc.GetLecs(depth);
		}

		assertOne(name, "TBracketR", depth);

		res.first = depth + 1;
		return res;
	}


	private int fparams(int depth, Tree tree) throws strException, castomExp {
		String name;
		--depth;
		int count = 0;
		Pair<Integer, Type> res;
		do {
			res = or(++depth);
			depth = res.first;
			name = sc.GetLecs(depth);
			if (checkContext) {
				Leaf leaf = getNum(tree, count);
				ExpAssing(leaf.type, res.second, depth);
			}
			count++;
		} while (Objects.equals(name, "TComma"));
		if (checkContext && count != tree.leafs.size() - 1)
			tcount(depth);
		return depth;
	}

	private Leaf getNum(Tree tree, int num) {
		for (Map.Entry<String, Leaf> stringLeafEntry : tree.leafs.entrySet()) {
			if (stringLeafEntry.getValue().num == num) {
				return stringLeafEntry.getValue();
			}
		}
		return null;
	}

	class Triple<A, B, C> {
		A first;
		B second;
		C trid;

		Triple(A first, B second, C trid) {
			this.first = first;
			this.second = second;
			this.trid = trid;
		}
	}

	class Pair<A, B> {
		A first;
		B second;

		Pair(A first, B second) {
			this.first = first;
			this.second = second;
		}
	}
}